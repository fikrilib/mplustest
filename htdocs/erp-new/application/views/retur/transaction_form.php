<style type="text/css">
	.table thead tr th{
		text-align: center;
	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;

	}
</style>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Input Retur Penjualan</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<form class="form-horizontal" method="POST" action="<?= base_url(); ?>panel/returnItem/listReturnTransaction/save">
							<div class="col-md-12">
								<div class="form-group">
									<div class="col-md-10 col-md-offset-1">
										<textarea class="form-control" name="return_description" placeholder="Keterangan"></textarea>
									</div>
								</div>
								<div class="form-group mt-30">
									<div class="col-md-4 col-md-offset-1">
										<select class="form-control select2" name="retur_item">
											<option value="0" selected="" disabled="">Pilih Barang</option>
											<?php foreach ($data_item as $data) { ?>
											<option value="<?= $data['item_id']; ?>" data-name="<?= $data['item_name']; ?>" data-price="<?= $data['item_sold_price']; ?>"><?= $data['item_number'];?> | <?= $data['item_name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-2">
										<input type="number" class="form-control" name="retur_qty" placeholder="Jumlah" min="1">
									</div>
									<div class="col-md-1">
										<a href="javascript: addItem()" class="btn btn-primary" ><i class="fa fa-plus"></i></a>
									</div>
								</div>
								<div class="form-group mt-30">
									<div class="col-md-10 col-md-offset-1">
										<table class="table table-hovered table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%">#</th>
													<th>Nama Barang</th>
													<th>Jumlah</th>
													<th style="width: 5%"><a href="javascript: deleteItem(0,0)" class="btn btn-danger"><i class="fa fa-trash"></i></a></th>
												</tr>
											</thead>
											<tbody id="result-data">
												
											</tbody>
										</table>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-2 col-md-offset-7">
										<a href="javascript:goBack()" class="btn btn-warning" style="width: 100%">Kembali</a>
									</div>
									<div class="col-md-2" style="padding-left: 0px">
										<input type="submit" class="btn btn-primary" style="width: 100%" value="Simpan" >
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	window.number = 0;
	window.arrayItem = [] ;
	$(document).ready(function() {
		$.fn.datepicker.defaults.format = "yyyy-mm-dd";
		$(".dateCustom").datepicker({ dateFormat: 'dd-mm-yy', autoclose: true, todayBtn: "linked", language: "id"});
	});
	function addItem(){
		var item_id =  $("select[name='retur_item']").val();
		var item_name =  $("select[name='retur_item']").find(':selected').data('name');
		var qty = $("input[name='retur_qty']").val().replace(/\D/g,'');

		var check = true;
		$.each(arrayItem, function(key,value) {
			if(item_name ==  value){
				check = false;             
			}
		})
		if (check === false) {
			alert("Barang sudah di inputkan!");
			return false;
		} else if(qty <= 0){
			alert("Jumlah barang kosong!");
			$("input[name='jumlah']").focus();
			return false;
		} else {
			++number;
			$("#result-data").append('<tr id="detail'+number+'"><td class="text-center"><b>'+number+'.</b></td><td><input type="hidden" name="item_id[]" value="'+item_id+'"><span>'+item_name+'</span></td><td class="text-center"><input type="hidden" name="detail_qty[]" value="'+qty+'"><span>'+qty+'</span</td><td><a href="javascript: deleteItem('+number+')" class="btn btn-danger"><i class="fa fa-times"></i></a></td></tr>');
			$( ".btn" ).addClass( "btn-flat" );
			arrayItem.push(item_name);
		}
		$("select[name='retur_item']").select2("val","0");
		$("input[name='retur_qty']").val("");
	}
	function deleteItem(id){
		if(id == 0){
			var confirm = window.confirm("Anda yakin akan menghapus semua barang?");
			if(confirm){
				$("#result-data").html("");
				window.arrayItem = [] ;
			}
		} else {
			var confirm = window.confirm("Anda yakin akan menghapus barang ini?");
			if(confirm){
				arrayItem.remove($("#detail"+id+" td span:first").text());
				$("#detail"+id).remove();
			}
		}
	}
	Array.prototype.remove = function(x) { 
		var i;
		for(i in this){
			if(this[i].toString() == x.toString()){
				this.splice(i,1)
			}
		}
	}
</script>