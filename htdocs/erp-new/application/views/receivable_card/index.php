<style type="text/css">
	.table thead tr th{
		text-align: center;
	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title pull-right">Kartu Piutang</h3>
					<form method="POST" action="">
						<h5 class="col-md-1 text-bold text-right">Tanggal</h5>
						<div class="col-md-4">
							<div class="input-group">
								<input class="form-control dateCustom" type="text" name="start_date" required>
								<span class="input-group-addon">s/d</span>
								<input class="form-control dateCustom" type="text" name="end_date" required>
							</div>
						</div>
						<h5 class="col-md-1 text-bold text-right">Member</h5>
						<div class="col-md-3">
							<select class="form-control select2" name="member_id">
								<?php foreach($member as $data) { ?>
								<option value="<?= $data['member_id'] ?>"><?= $data['member_name'] ?></option>
								<?php } ?>
							</select>
						</div>
						<div class="col-md-1">
							<button type="submit" class="btn btn-primary">Go</button>
						</div>
					</form>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th width="10%">Tanggal</th>
										<th width="10%">Keterangan</th>
										<th width="10%">Masuk</th>
										<th width="10%">Keluar</th>
										<th width="10%">Saldo</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<?php
									
									?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= indonesianDate($data["date"]) ?></td>
										<td><?= $data["description"] ?></td>
										<td>Rp <?= number_format($data["in"],0, ",","."); ?></td>
										<td>Rp <?= number_format($data["out"],0, ",","."); ?></td>
										<td>Rp <?= number_format($data["saldo"],0, ",","."); ?></td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(document).ready(function() {
		$.fn.datepicker.defaults.format = "yyyy-mm-dd";
		$(".dateCustom").datepicker({ dateFormat: 'dd-mm-yy', autoclose: true, todayBtn: "linked", language: "id"});
	});
</script>