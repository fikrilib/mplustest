<style type="text/css">
    .table thead tr th{
        text-align: center;
    }
    .table thead tr th, .table tbody tr td, .table tfoot tr th{
        vertical-align: middle;
        cursor: pointer;

    }
</style>
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-orange">
	            <div class="box-header">
	            	<h3 class="box-title text-uppercase" style="font-weight: bold">Kasir - <?php echo $this->session->userdata("user")?></h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn bg-red btn-box-tool btn-off"   onclick="PrintDivKasir();"><i class="fa fa-power-off fa-2x"></i></button>
                    </div>
	            </div>
	            <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?=$this->session->userdata('message_action') ?>
                        </div>
                    </div>
                    <div class="col-md-8" style="border-right: 1px solid #bbb; height: 100vh">
    	            	<form method="POST" action="" id="myForm">
                            <div class="form-group">
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <select class="form-control select2" name="member_id" onchange="getMemberName(this.value)" required="">
                                        <option value="0" selected="" disabled="">Pilih Anggota</option>
                                        <?php foreach ($data_member as $data) { ?>
                                        <option value="<?= $data['member_id']; ?>" data-id="<?= $data['member_id']; ?>" data-name="<?= $data['member_name']; ?>"><?= $data['member_number'];?> | <?= $data['member_name']; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="member_name" readonly="" placeholder="Nama Anggota">
                                </div>
                            </div>
    	            		<div class="col-lg-12" style="margin-bottom: 20px;">
    						<input id="kodene" type="text" name="n_barang" class="form-control" autocomplete="off" autofocus="autofocus" placeholder="Kode" required="required" onkeypress="onEnter(event)">
    	            		</div>
    	            	</form>
    	            	<!-- List Barang -->
    					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-top: 20px">
    						<div class="table-responsive">
                                <form name="cart"  method="POST" action="<?=site_url("TransactionCash/selesai")?>">
                                <table id="myTable" class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Kode</th>
                                                <th>Nama</th>
                                                <th>Quantity</th>
                                                <th>Harga</th>
                                                <th>Diskon</th>
                                                <!-- <th><button onclick="hapusSemua()" class="btn btn-danger hapus-semua btn-flat"><i class="fa fa-trash-o"></i>&nbsp;Hapus Semua</button>
                                            </th> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <div class="tampilkan"></div>
                                    </tbody>
                                </table>
    						</div>
    					</div>
                    </div>
					<!--Modal-->
	            	<div class="col-lg-4">
	            		<h2 class="text-uppercase">Total Pembayaran</h2>
                        <div style="background-color: #1087BF; color: #fff; text-align: center; padding: 3px 0;">
	            		 <h1><div class="totalan" id="totalcuk"></h1>
                        </div>
					       <div class="form-group">
                                    <div class="input-group" style="margin-top: 20px">
                                        <div class="input-group-addon">
            								<input type="checkbox" id="aktif_diskon" name="aktif_diskon" onClick="aktifkan_diskon()"> 
                                        </div>
        								<input type="text" id="diskon_seluruh" name="diskon_seluruh" class="form-control" placeholder="Diskon (%)" >
                                    </div>
							</div>

                            

                            <div class="form-group">
								<label style="">Bayar Tunai </label>
								<input type="text" id="bayare" style="width:100%; margin-top:5px;" name="bayar_tunai" class="form-control"  placeholder="Bayar" required>
							</div>

                            <div class="form-group" id="tr_kembali">
								<h1 class="text-uppercase">Kembali</h1>
								<div style="background-color: #ea5f51; color: #fff; text-align: center; padding: 3px 0;">
                                 <h1><div class="kembali" id="kembaliee">0</div></h1>
                                 <input type="hidden" id="kembalie" class="kembalie" name="kembali">
                                </div>
							</div>
							<div class="form-group">
                                <label style=" margin-top:5px;">Keterangan</label></td>
                                <textarea name="keterangan" class="form-control" style=" margin-top:5px;"></textarea>
                            </div>
                            <input type="hidden" name="total_2" id="total_2" class="total_2" value="">
                            <input type="hidden" name="total_thok" id="total_thok" class="total_thok" value="">
							<div class="form-group">
								<div class="pull-right">
                                    <!-- <b class="btn btn-success btn-flat btn-lg" onclick="PrintDiv();">PRINT</b> -->
                                    <input type="submit" class="btn btn-success btn-flat btn-lg" value="SELESAI">
                                </div>

							</div>
						</form>
	            	</div>
	            </div>
            </div>
        </div>
    </div>
</section>
 <script>
        function getMemberName(value){
            var member = $("select[name='member_id']").find(':selected').data('name');
            $("input[name='member_name']").val(member);
        }
        
    $(document).ready(function(){
        $("body").addClass("sidebar-collapse");
        document.cart.diskon_seluruh.disabled = true;          

        $('#tombol').click(function() {
            $(this).addClass('disabled');
            konfirmasi();
        });

        $('#diskon_seluruh').keyup(function() {
              calculateDisconTotal();
          });
      

        $('#ongkir').keyup(function() {
                ongkir = parseInt($('#ongkir').val());
                total = parseInt($('#total_2').val());
                total2 = total + ongkir ;
                $("#total, .totalan").html(accounting.formatNumber(total2, 0, ".", ","));
                $('#total_3').val(total2);
         });

        $('#bayare').keyup(function() {
            
                bayare = parseInt($('#bayare').val());
                total = parseInt($('#total_2').val());
                kembali = bayare - total;
                 $('#kembalie').val(kembali);
                 $('#kembaliee').html(kembali);
            
        });
        kolom();
        total();
        faktur();
        $('#ganti');

        $('#kembalian').click(function() {
            site_url = '<?=site_url()?>';
            $.get(site_url+'TransactionCash/total', function(data) {
                tot = data;
                bayare = $('#bayare').unmask();
                kembali = bayare - tot;
                $('#ganti').html('<h4 class="totalan">'+kembali+'</h4>');
            });
        });

        $('.tutup').click(function() {
            /* Act on the event */

        });
        $("#tunai").hide();
                    $("#non").hide();
    });

    function aktifkan_diskon(){
        if(document.cart.aktif_diskon.checked == true ){
                document.cart.diskon_seluruh.disabled = false;   
            }   
        else{
                document.cart.diskon_seluruh.disabled = true;
            }
    }

    function aktifkan_customer(){
        if(document.cart.aktif_customer.checked == true ){
                document.cart.nama_customer.disabled = false;   
            }   
        else{
                document.cart.nama_customer.disabled = true;
            }
    }
        

    function kolom()
    {
      site_url = "<?=site_url('TransactionCash/daftarkeranjang')?>";
      $.get(site_url, function(data) {
        $(".keranjang").html(data);
      });
    }
    function faktur()
    {
      site_url = "<?=site_url('TransactionCash/faktur')?>";
      $.get(site_url, function(data) {
        $(".faktur").html(data);
      });
    }

    function total()
    {
      site_url = '<?=site_url()?>';
      $.get(site_url+'TransactionCash/total', function(data) {
        $("#total, .totalan").html(accounting.formatNumber(data, 0, ".", ","));
        $("#total_2, .total_2").val(data);
        $("#total_3, .total_3").val(data);
      });
    }

    function konfirmasi()
    {
        setTimeout(function(){
          site_url = '<?=site_url()?>';
          var qty_manual = $("#qty_manual").val();
          var diskon = $("#diskon").val();
          var kode = $("#manual").val();
          if(qty_manual==''){
            qty_manual2=1;
          }
          else{
            qty_manual2=qty_manual;
          } 
          $.get(site_url+'Cart/keranjang/'+kode+'/'+qty_manual2+'/'+diskon, function(a) {
            /*optional stuff to do after success */
            $("#manual").val('');
            $("#qty_manual").val('');
            $("#n_barang").val('');
            $("#diskon").val('');
            kolom();
            total();
            faktur();

          }).done(function() {
            $("#tombol").removeClass('disabled');
          });
          //$('#form').submit();
        }, 30);
    }

    function hapusSemua()
    {
        site_url = '<?=site_url()?>';
        $.get(site_url+'Cart/delete', function() {
            /*optional stuff to do after success */
            kolom();
            total();    
        });
    }
</script>

<script>

    function delete_row (kode){
    var site_url = '<?=site_url()?>';
    $.get(site_url+'Cart/deleterow/'+kode, function(data) {
        /*optional stuff to do after success */
        kolom();
        total();
        });
    }
</script>
<script>
var i = 0;
function onEnter(e){
    // var barcode = document.getElementById('kodene').value;
    var barcode=$('#kodene').val();
    var key=e.keyCode || e.which;
    if(key==13){
            
        // alert(barcode);
              $.ajax({
              type:"post",
              url:"<?=base_url();?>TransactionCash/barcode",
              data:'barcode='+barcode,
              dataType: 'json',
              success:function(html){
                    if(html.hasil == 'sukses'){
                        i = $('#myTable tbody tr').length + 1;
                        $('#myTable tbody').append('<tr id="tbl_'+ i +'"><td>'+ i +'<input type="hidden" class="form-control" name="kd_barang[]" style="width:50px;" value='+ html.kode +'><input type="hidden" class="form-control" name="id_barang[]" style="width:50px;" value='+ html.id_barang +'></td><td>'+ html.nama +'</td><td><input type="text" class="form-control qty_'+ i +'" name="qty[]" onkeyup="calculateDisconTotal()" style="width:50px;" value="1"></td><td>Rp '+ html.harga_jual +'<input type="hidden" class="form-control harga_'+ i +'" id="harga" name="price[]" style="width:50px;" value='+ html.harga +'></td><td><input type="text" class="form-control diskon_'+ i +'" name="diskon[]" onkeyup="calculateDisconTotal()" style="width:50px;" value="0"></td><td><button type="button" id="ibtnDel" class="btn btn-danger">delete</button></td></tr>');
                            calculateDisconTotal();
                    }else{
                        alert('tidak ada data');
                    }
                }
              });
        $("#myForm")[0].reset();
    }

}

$("table#myTable").on("click", "#ibtnDel", function (event) {
    $(this).closest("tr").remove();
    calculateDisconTotal();
    $('#addrow').attr('disabled', false).prop('value', "Add Row");
});

// function calculateGrandTotal() {
//     var total = 0;
//     $('#myTable tbody').find("#harga").each(function() {
//     total += parseInt($(this).val());
//     });
//     $('#totalcuk').html(total);
// }

function calculateGrandTotal() {
    var count = $('#myTable tbody tr').length;
    var total = 0;
    var totale = 0;
    $('#myTable tbody').find("#harga").each(function() {
        totale = 0;
        for(x = 1; x <= count; ++x){
            totale += (parseInt($('.qty_'+ x).val()) * parseInt($('.harga_'+ x).val())) - ((parseInt($('.qty_'+ x).val()) * parseInt($('.harga_'+ x).val())) *parseInt($('.diskon_'+ x).val())/100);
        }
    });
    $('#totalcuk').html(totale);
    $('#total_2').val(totale);
}

function calculateDisconTotal() {
    var total = 0;
    var discon = 0;
    var diskon_seluruh = $('#diskon_seluruh').val();
    var count = $('#myTable tbody tr').length;
    $('#myTable tbody').find("#harga").each(function() {
        totale = 0;
        for(x = 1; x <= count; ++x){
            totale += (parseInt($('.qty_'+ x).val()) * parseInt($('.harga_'+ x).val())) - ((parseInt($('.qty_'+ x).val()) * parseInt($('.harga_'+ x).val())) *parseInt($('.diskon_'+ x).val())/100);
        }
    });
    discon = (totale - (totale * (diskon_seluruh/100)));
    $('#totalcuk').html(discon);
    $('#total_2').val(discon);
    $('#total_thok').val(totale);
}
</script>
