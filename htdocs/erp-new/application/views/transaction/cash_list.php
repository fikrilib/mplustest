<style type="text/css">
	.table thead tr th{
		text-align: center;
	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;
	}
</style>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Riwayat Penjualan Tunai</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Tanggal</th>
										<th>Nomor Beli</th>
										<th>Anggota</th>
										<th>Jumlah Bayar</th>
										<th style="width: 100px;">Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= indonesianDate($data["transaction_date"]) ?></td>
										<td><?= $data["transaction_number"] ?></td>
										<td><?= $data["member_name"] ?></td>
										<td>Rp <?= number_format($data['transaction_grand_total'],0, ",","."); ?></td>
										<td>
											<a href="javascript:detail(<?= $data['transaction_id']; ?>)" id="detail<?= $data['transaction_id']; ?>" data-number="<?= $data["transaction_number"] ?>" data-date="<?= indonesianDate($data["transaction_date"]) ?>" data-member="<?= $data["member_name"] ?>" data-total="<?= $data["transaction_total"] ?>" data-discount="<?= intVal($data["transaction_discount"]) ?>" data-grand="<?= $data["transaction_grand_total"] ?>" data-toggle="tooltip" data-placement="top" title="Detail <?= $data["transaction_number"] ?>" class="btn btn-sm btn-success" style="width: 50%"><i class="fa fa-info"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Detail Riwayat Penjualan Tunai </h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="#">
					<div class="form-group">
						<label class="col-md-3 control-label">Nomor Penjualan</label>
						<h5 class="col-md-2" id="transaction_number">:</h5>
						<label class="col-md-3 control-label">Tanggal</label>
						<h5 class="col-md-3" id="transaction_date">:</h5>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Anggota</label>
						<h5 class="col-md-3" id="member_name">:</h5>
					</div>
					<div class="form-group">
						<div class="col-md-10 col-md-offset-1">
							<table class="table table-hovered table-bordered table-striped">
								<thead>
									<tr>
										<th style="width: 5%">#</th>
										<th>Nama Barang</th>
										<th>Jumlah</th>
										<th>Harga</th>
										<th width="20%">Sub Total</th>
									</tr>
								</thead>
								<tbody id="result-data">

								</tbody>
								<tfoot>
									<tr>
										<th colspan="4" class="text-right">Total</th>
										<th><span id="transaction_total">Rp 0</span></th>
									</tr>
									<tr>
										<th colspan="4" class="text-right">Diskon</th>
										<th colspan="2"><span id="transaction_discount"></span></th>
									</tr>
									<tr>
										<th colspan="4" class="text-right">Jumlah Bayar</th>
										<th><span id="transaction_grand_total">Rp 0</span></th>
									</tr>
								</tfoot>
							</table>							
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<a data-dismiss="modal" class="btn btn-warning btn-flat">Tutup</a>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		
	});
	function detail(id){
		$.ajax({
			url: '<?= base_url(); ?>panel/transaction/cash/listCash/detail/'+id,
			beforeSend: function(){
				$("#transaction_number").text(": "+$("#detail"+id).data("number"));
				$("#transaction_date").text(": "+$("#detail"+id).data("date"));
				$("#member_name").text(": "+$("#detail"+id).data("member"));
				$("#transaction_total").text(toRp($("#detail"+id).data("total")));
				$("#transaction_discount").text($("#detail"+id).data("discount")+" %");
				$("#transaction_grand_total").text(toRp($("#detail"+id).data("grand")));
				$("#result-data").html('<tr><td colspan="5" class="text-center"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span></td></tr>');
				$("#formModal").modal("show");
			},
			success: function(response){
				$("#result-data").html("");
				var data = $.parseJSON(response);
				$.each(data.detail_transaction, function(key,value) {
					$("#result-data").append('<tr><td class="text-center"><b>'+value.number+'.</b></td><td>'+value.name+'</td><td class="text-center">'+value.qty+'</td><td>'+toRp(value.price)+'</td><td>'+toRp(value.sub_total)+'</td></tr>');
					
				})

			}
		})
	}
	
</script>