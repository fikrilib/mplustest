<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Anggota</h3>
					<div class="pull-right">
						<a type="button" href="javascript:addNew()" class="btn btn-primary">Tambah Data</a>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Nomor Anggota</th>
										<th>Nama</th>
										<th>Telepon</th>
										<th>Alamat</th>
										<th style="width: 100px;">Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= $data["member_number"] ?></td>
										<td><?= $data["member_name"] ?></td>
										<td><?= $data["member_phone"] ?></td>
										<td><?= $data["member_address"] ?></td>
										<td>
											<a href="javascript:update(<?= $data['member_id']; ?>)" id="update<?= $data['member_id']; ?>" data-number="<?= $data["member_number"] ?>" data-name="<?= $data["member_name"] ?>" data-address="<?= $data["member_address"] ?>" data-phone="<?= $data["member_phone"] ?>" data-toggle="tooltip" data-placement="top" title="Edit <?= $data["member_name"] ?>" class="btn btn-sm btn-warning btn-flat"><i class="fa fa-edit"></i></a>
											<a href="<?= base_url()?>panel/member/listMember/delete/<?= $data['member_id']; ?>" onclick="return confirm('Yakin akan menghapus data <?= $data["member_name"] ?>?')" data-toggle="tooltip" data-placement="top" title="Hapus <?= $data["member_name"] ?>" class="btn btn-sm btn-danger btn-flat"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Input Data Anggota</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="<?= base_url()?>panel/member/listMember/save"  method="post">
					<div class="form-group">
						<label class="col-md-3 control-label">Nomor Anggota</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="member_number" placeholder="Nomor Anggota" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Nama</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="member_name" placeholder="Nama" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Telepon</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="member_phone" placeholder="Telepon" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Alamat</label>
						<div class="col-md-7">
							<textarea class="form-control" name="member_address" placeholder="Alamat" required></textarea>							
						</div>
					</div>
					<input type="hidden" name="id" id="id" />
				</div>
				<div class="modal-footer">
					<a data-dismiss="modal" class="btn btn-warning btn-flat">Batal</a>
					<button type="submit" id="submit-form" class="btn btn-primary btn-flat">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#formModal').on('shown.bs.modal', function () {
			$("input[name='member_number']").focus();
		})  
	});
	function addNew(){
		$("input[name='member_number']").val("");
		$("input[name='member_name']").val("");
		$("input[name='member_phone']").val("");
		$("textarea[name='member_address']").val("");
		$("#myModalLabel").text("Input Data Anggota");
		$("#id").val(0);
		$("#submit-form").text("Simpan");
		$("#formModal").modal("show");
	}
	function update(id){
		$("input[name='member_number']").val($("#update"+id).data("number"));
		$("input[name='member_name']").val($("#update"+id).data("name"));
		$("input[name='member_phone']").val($("#update"+id).data("phone"));
		$("textarea[name='member_address']").val($("#update"+id).data("address"));
		$("#myModalLabel").text("Edit Data Anggota");
		$("#id").val(id);
		$("#submit-form").text("Edit");
		$("#formModal").modal("show");
	}
</script>