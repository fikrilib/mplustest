<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Masuk | Koperasi Panel</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" href="<?=base_url();?>assets/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/dist/css/AdminLTE.min.css">
	<link rel="stylesheet" href="<?=base_url();?>assets/plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="#"><b>Koperasi</b>Panel</a>
		</div>
		<div class="login-box-body">
			<p class="login-box-msg">Masuk Untuk Menggunakan Aplikasi</p>

			<form action="<?= base_url(); ?>panel/login/auth" method="post">
				<div class="form-group has-feedback">
					<input type="text" class="form-control" placeholder="Username" name="username">
					<span class="fa fa-user form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Password" name="password">
					<input type="hidden" name="login" required class="form-control" value='yes'/>
					<span class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<div class="col-xs-7">
						<div class="checkbox icheck">
							<label>
								<input type="checkbox"> Ingat Saya
							</label>
						</div>
					</div>
					<div class="col-xs-5">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	<script src="<?=base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
	<script src="<?=base_url();?>assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?=base_url();?>assets/plugins/iCheck/icheck.min.js"></script>
		<script>
			$(function () {
				$('input').iCheck({
					checkboxClass: 'icheckbox_square-blue',
					radioClass: 'iradio_square-blue',
					increaseArea: '20%'
				});
			});
		</script>
	</body>
	</html>