
<!DOCTYPE html>
<html lang="en" data-textdirection="LTR" class="loading">
  
<!-- Mirrored from demo.pixinvent.com/robust-admin/ltr/vertical-content-menu-template/register-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Feb 2017 08:14:17 GMT -->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="Robust admin is super flexible, powerful, clean &amp; modern responsive bootstrap 4 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, robust admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="PIXINVENT">
    <title>Register Page - Robust Admin Template</title>
    <link rel="apple-touch-icon" sizes="60x60" href="<?=base_url();?>assets/ico/apple-icon-60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url();?>assets/ico/apple-icon-76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=base_url();?>assets/ico/apple-icon-120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=base_url();?>assets/ico/apple-icon-152.png">
    <link rel="shortcut icon" type="image/x-icon" href="<?=base_url();?>assets/ico/favicon.ico">
    <link rel="shortcut icon" type="image/png" href="<?=base_url();?>assets/ico/favicon-32.png">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <!-- BEGIN VENDOR CSS-->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/vendors.min.css"/>
    <!-- BEGIN VENDOR CSS-->
    <!-- BEGIN Font icons-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/fonts/icomoon.css">
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/fonts/flag-icon-css/css/flag-icon.min.css">
    <!-- END Font icons-->
    <!-- BEGIN Plugins CSS-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/css/plugins/sliders/slick/slick.css">
    <!-- END Plugins CSS-->
    
    <!-- BEGIN Vendor CSS-->
    <!-- END Vendor CSS-->
    <!-- BEGIN ROBUST CSS-->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/app.min.css"/>
    <!-- END ROBUST CSS-->
    <!-- BEGIN Page Level CSS-->
    <!-- END Page Level CSS-->
    <!-- BEGIN Custom CSS-->
    <link rel="stylesheet" type="text/css" href="<?=base_url();?>assets/style.css">
    <!-- END Custom CSS-->
  </head>
  <body data-open="click" data-menu="vertical-content-menu" data-col="1-column" class="vertical-layout vertical-content-menu 1-column  blank-page blank-page">
    <!-- START PRELOADER-->

    <!-- END PRELOADER-->
    <!-- ////////////////////////////////////////////////////////////////////////////-->
    <div class="robust-content content container-fluid">
      <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body"><section class="flexbox-container">
    <div class="col-md-4 offset-md-4 col-xs-10 offset-xs-1 box-shadow-2 p-0">
		<div class="card border-grey border-lighten-3 px-2 py-2 m-0">
			<div class="card-header no-border">
				<div class="card-title text-xs-center">
					<img src="<?=base_url();?>assets/images/logo/robust-logo-dark.png" alt="branding logo">
				</div>
				<h6 class="card-subtitle line-on-side text-muted text-xs-center font-small-3 pt-2"><span>Create Account</span></h6>
			</div>
			<div class="card-body collapse in">	
				<div class="card-block">
					<form class="form-horizontal form-simple" action="" method="POST">
						<fieldset class="form-group has-feedback has-icon-left mb-1">
							<input type="text" class="form-control form-control-lg input-lg" id="user-name" placeholder="User Name" name="username">
							<div class="form-control-position">
							    <i class="icon-head"></i>
							</div>
						</fieldset>
                        <fieldset class="form-group has-feedback has-icon-left mb-1">
                            <input type="email" class="form-control form-control-lg input-lg" id="user-email" placeholder="Email Anda" name="email" required>
                            <div class="form-control-position">
                                <i class="icon-mail6"></i>
                            </div>
                        </fieldset>
                        <fieldset class="form-group has-feedback has-icon-left mb-1">
                            <input type="text" class="form-control form-control-lg input-lg" id="user-email" placeholder="NIS Anak Anda" name="nis" required>
                            <div class="form-control-position">
                                <i class="icon-mail6"></i>
                            </div>
                        </fieldset>
                        <input type="hidden" name="register" value="yes">
						<fieldset class="form-group has-feedback has-icon-left">
							<input type="password" class="form-control form-control-lg input-lg" id="user-password" placeholder="Password Anda" name="password" required>
							<div class="form-control-position">
							    <i class="icon-key3"></i>
							</div>
						</fieldset>
						<button type="submit" class="btn btn-primary btn-lg btn-block"><i class="icon-unlock2"></i> Register</button>
					</form>
				</div>
				<p class="text-xs-center">Already have an account ? <a href="<?=base_url();?>Admin/login" class="card-link">Login</a></p>
			</div>
		</div>
	</div>
</section>
        </div>
      </div>
    </div>
    <!-- ////////////////////////////////////////////////////////////////////////////-->

    <!-- BEGIN VENDOR JS-->
    <script src="<?=base_url();?>assets/ js/vendors.min.js"></script>
    <!-- BEGIN VENDOR JS-->
    <!-- BEGIN PAGE VENDOR JS-->
    <script src="<?=base_url();?>assets/ js/plugins/ui/headroom.min.js" type="text/javascript"></script>
    <script src="<?=base_url();?>assets/ js/plugins/forms/validation/jqBootstrapValidation.js" type="text/javascript"></script>
    <!-- END PAGE VENDOR JS-->
    <!-- BEGIN ROBUST JS-->
    <script src="<?=base_url();?>assets/ js/app.min.js"></script>
    <!-- END ROBUST JS-->
    <!-- BEGIN PAGE LEVEL JS-->
    <script src="<?=base_url();?>assets/ js/components/forms/form-login-register.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS-->
    </body>

<!-- Mirrored from demo.pixinvent.com/robust-admin/ltr/vertical-content-menu-template/register-simple.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 08 Feb 2017 08:14:17 GMT -->
</html>