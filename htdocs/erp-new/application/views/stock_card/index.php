<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title pull-right">Kartu Persediaan</h3>
					<form method="POST" action="">
						<h5 class="col-md-1 text-bold text-right">Tanggal</h5>
						<div class="col-md-4">
							<div class="input-group">
								<input class="form-control date" type="text" name="start_date">
								<span class="input-group-addon">s/d</span>
								<input class="form-control date" type="text" name="end_date">
							</div>
						</div>
						<h5 class="col-md-1 text-bold text-right">Barang</h5>
						<div class="col-md-4">
							<select class="form-control select2" name="item_id">
								<?php foreach($array_goods as $data) { ?>
								<option value="<?= $data['item_id'] ?>"><?= $data['item_name'] ?></option>
								<?php } ?>
							</select>
						</div>
					</form>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							
						</div>
					</div>
					<div class="row mt-30">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="customTable">
								<thead>
									<tr>
										<th rowspan="2">Tanggal</th>
										<th rowspan="2">Keterangan</th>
										<th rowspan="2">Nomor Transaksi</th>
										<th colspan="3">Masuk</th>
										<th colspan="3">Keluar</th>
										<th colspan="3">Saldo</th>
									</tr>
									<tr>
										<th>Jumlah</th>
										<th>Harga</th>
										<th>Total</th>
										<th>Jumlah</th>
										<th>Harga</th>
										<th>Total</th>
										<th>Jumlah</th>
										<th>Harga</th>
										<th>Total</th>
									</tr>
								</thead>
								<tbody>
									
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(document).ready(function() {
		$.fn.datepicker.defaults.format = "yyyy-mm-dd";
		// $(".date3").datepicker({ dateFormat: 'dd-mm-yy', autoclose: true, todayBtn: "linked", language: "id"});
	});
</script>
<script type="text/javascript">
	$(document).ready(function() {
		// $("body").addClass("sidebar-collapse");
		window.table = $("#customTable").DataTable({
			"bPaginate": false,
			"bFilter": false,
			"bInfo": false,
			"ajax": {
				url: '<?= base_url(); ?>panel/items/stockCard/load',
				type: 'POST',
				data: function ( d ) {
					d.start_date = $("input[name='start_date']").val();
					d.end_date = $("input[name='end_date']").val();
					d.item_id = $("select[name='item_id']").val();
				},
			},
			"oLanguage": {
				"sProcessing": "Memuat Data...",
			},
			"processing" : true,
			"createdRow": function ( row, data, index ) {
				$('td', row).eq(0).css({'vertical-align': 'middle', 'text-align': 'center','font-weight': 'bold','width': '10%' });
				$('td', row).eq(1).css({'vertical-align': 'middle' }); 
				$('td', row).eq(2).css({'vertical-align': 'middle', }); 
				$('td', row).eq(3).css({'vertical-align': 'middle' }); 
				$('td', row).eq(4).css({'vertical-align': 'middle', 'font-weight': 'bold', 'text-align': 'center' }); 
				$('td', row).eq(5).css({'vertical-align': 'middle', 'font-weight': 'bold', 'text-align': 'center' }); 
				$('td', row).eq(6).css({'vertical-align': 'middle', 'font-weight': 'bold', 'text-align': 'right' }); 
				$('td', row).eq(7).css({'vertical-align': 'middle', 'font-weight': 'bold', 'text-align': 'right' }); 
				$('td', row).eq(8).css({'vertical-align': 'middle', 'font-weight': 'bold', 'text-align': 'right' }); 
				$('td', row).eq(9).css({'vertical-align': 'middle', 'font-weight': 'bold', 'text-align': 'center' });
				$('td', row).eq(10).css({'vertical-align': 'middle', 'font-weight': 'bold', 'text-align': 'center' });
			}
		});
		$("select[name='item_id']").change(function(){
			table.ajax.reload();
		})
		$("input[name='start_date'], input[name='end_date']").datepicker().on('changeDate', function (ev) {
			table.ajax.reload();
			$(this).datepicker('hide');
		});
	});

</script>