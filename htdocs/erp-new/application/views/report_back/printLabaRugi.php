<style type="text/css">
@page {
  size: A4 ;
}
	.table thead tr th{
		text-align: center;

	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;
		padding:3px 3px 3px 3px;
	}
	.table th td{

	}

tr.group,
tr.group:hover {
    background-color: #ddd !important;
}
.box{
    border-collapse: collapse;
    border-width: 1px;
    border-style: solid
}
</style>
<?php
	$array_tanggal = array();
	$array_transaction_number = array();
	// $array_kode_barang = array();
	$array_nama_barang = array();
	$array_detail_qty = array();
	$array_harga_beli = array();
	$array_harga_jual = array();
	$array_diskon = array();
	$array_subtotal = array();
	$array_labarugi = array();
	$array_rowspan = array();
	$tempid = array();
	$i = 0;
	foreach ($data_array as $data) {
	    if($tempid != $data['transaction_number']){

	        $tempid = $data['transaction_number'];
	        $rowspan = 1;       
			$array_tanggal[] = $data['tanggal'];
			$array_transaction_number[] = $data['transaction_number'];
			// $array_kode_barang[] = $data['kode_barang'];
			$array_nama_barang[] = $data['nama_barang'];
			$array_detail_qty[] = $data['detail_qty'];
			$array_harga_beli[] = $data['harga_beli'];
			$array_harga_jual[] = $data['harga_jual'];
			$array_diskon[] = $data['diskon'];
			$array_subtotal[] = $data['subtotal'];
			$array_labarugi[] = $data['labarugi'];        
	        $array_rowspan[$i] = $rowspan;

	        //Set another temp value to set same rowspan in mached category id  
	        $same_id=1; 
	    }else{

	        $rowspan++;     
			$array_tanggal[] = $data['tanggal'];
			$array_transaction_number[] = $data['transaction_number'];
			// $array_kode_barang[] = $data['kode_barang'];
			$array_nama_barang[] = $data['nama_barang'];
			$array_detail_qty[] = $data['detail_qty'];
			$array_harga_beli[] = $data['harga_beli'];
			$array_harga_jual[] = $data['harga_jual'];
			$array_diskon[] = $data['diskon'];
			$array_subtotal[] = $data['subtotal'];
			$array_labarugi[] = $data['labarugi']; 

	        //Set another temp value to set same rowspan in mached category id          
	        $same_id++;     
	        for($j=0; $j<$same_id; $j++){
	            $array_rowspan[$i-($j)] = $rowspan;
	        }

	    }       

	    $i++;  
	}

?>
<center>
	<h2>Laporan Laba Rugi Per Transaksi</h2>
	<h3 style="margin-bottom: 10px;">Periode <?=namabulan_2($this->uri->segment(5)).' '.$this->uri->segment(6);?></h3>
</center>
<table class="table box" id="example" border="1">
	<thead>
		<tr>
			<th>Tanggal</th>
			<th>No. Transaksi</th>
<!-- 			<th>Kode Barang</th> -->
			<th>Nama Barang</th>
			<th>Qty</th>
			<th>Harga Beli</th>
			<th>Harga Jual</th>
			<th>Diskon</th>
			<th>Subtotal</th>
			<th>Laba/Rugi</th>
		</tr>
	</thead>
	<tbody>
		<?php $tempid2 = ""; $sumPurchasePrice = 0; $sumSoldPrice = 0; $sumSubtotal = 0; $sumProfitLoss = 0; $start = 0; 
			foreach ($array_transaction_number as $key => $val) { 
				if($tempid2 != $val){
					$tempid2 = $val;
		?>
		<tr>
			<td><?= $array_tanggal[$key] ?></td>
			<td rowspan="<?=$array_rowspan[$key];?>"><?= $array_transaction_number[$key] ?></td>
			<!-- <td><?= $array_kode_barang[$key] ?></td> -->
			<td><?= $array_nama_barang[$key] ?></td>
			<td><?= $array_detail_qty[$key] ?></td>
			<td>Rp <?= number_format($array_harga_beli[$key],0, ",","."); ?></td>
			<td>Rp <?= number_format($array_harga_jual[$key],0, ",","."); ?></td>
			<td><?= $array_diskon[$key] ?> %</td>
			<td>Rp <?= number_format($array_subtotal[$key],0, ",","."); ?></td>
			<td>Rp <?= number_format($array_labarugi[$key],0, ",","."); ?></td>
		</tr>
		<?php 
				}else{
					?>
					<tr>
						<td><?= $array_tanggal[$key] ?></td>
						<!-- <td><?= $array_kode_barang[$key] ?></td> -->
						<td><?= $array_nama_barang[$key] ?></td>
						<td><?= $array_detail_qty[$key] ?></td>
						<td>Rp <?= number_format($array_harga_beli[$key],0, ",","."); ?></td>
						<td>Rp <?= number_format($array_harga_jual[$key],0, ",","."); ?></td>
						<td><?= $array_diskon[$key] ?> %</td>
						<td>Rp <?= number_format($array_subtotal[$key],0, ",","."); ?></td>
						<td>Rp <?= number_format($array_labarugi[$key],0, ",","."); ?></td>
					</tr>
			<?php
				}
			$sumSoldPrice += $array_harga_jual[$key];
			$sumPurchasePrice += $array_harga_beli[$key];
			$sumSubtotal += $array_subtotal[$key];
			$sumProfitLoss += $array_labarugi[$key];
		} ?>
	</tbody>
	<tfoot>
		<th></th>
		<th></th>
		<th></th>
		<th></th>
		<th>Rp <?=number_format($sumPurchasePrice,0, ",",".");?></th>
		<th>Rp <?=number_format($sumSoldPrice,0, ",",".");?></th>
		<th></th>
		<th>Rp <?=number_format($sumSubtotal,0, ",",".");?></th>
		<th>Rp <?=number_format($sumProfitLoss,0, ",",".");?></th>
	</tfoot>
</table>	