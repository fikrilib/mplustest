<style type="text/css">
	.table thead tr th{
		text-align: center;
	}
	.table thead tr th, .table tbody tr td, .table tfoot tr th{
		vertical-align: middle;
		cursor: pointer;

	}
</style>
<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Input Pembelian</h3>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<form class="form-horizontal" method="POST" action="<?= base_url(); ?>panel/purchasing/formPurchasing/save">
							<div class="col-md-12">
								<div class="form-group">
									<div class="col-md-4 col-md-offset-1">
										<input type="text" class="form-control" name="purchasing_number" placeholder="Nomor Beli" readonly value="<?=$purchasing_number;?>">
									</div>
									<div class="col-md-3">
										<input type="text" class="form-control dateCustom" name="purchasing_date" placeholder="Tanggal Beli" required="">
									</div>
									<div class="col-md-3">
										<select class="form-control select2" name="supplier_id" required="">
											<option value="0" selected="" disabled="">Pilih Supplier</option>
											<?php foreach ($data_supplier as $data) { ?>
											<option value="<?= $data['supplier_id']; ?>"><?= $data['supplier_name'];?></option>
											<?php } ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-4 col-md-offset-1">
										<select class="form-control select2" name="member_id" onchange="getMemberName(this.value)" required="">
											<option value="0" selected="" disabled="">Pilih Anggota</option>
											<?php foreach ($data_member as $data) { ?>
											<option value="<?= $data['member_id']; ?>" data-id="<?= $data['member_id']; ?>" data-name="<?= $data['member_name']; ?>"><?= $data['member_number'];?> | <?= $data['member_name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control" name="member_name" readonly="" placeholder="Nama Anggota">
									</div>
								</div>
								<div class="form-group mt-30">
									<div class="col-md-4 col-md-offset-1">
										<select class="form-control select2" name="purchasing_item" onchange="getItemName(this.value)">
											<option value="0" selected="" disabled="">Pilih Barang</option>
											<?php foreach ($data_item as $data) { ?>
											<option value="<?= $data['item_id']; ?>" data-name="<?= $data['item_name']; ?>" data-price="<?= $data['item_purchase_price']; ?>"><?= $data['item_number'];?> | <?= $data['item_name']; ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="col-md-2">
										<input type="number" class="form-control" name="purchasing_qty" placeholder="Jumlah" onkeyup="countSubTotal()" onmouseup="countSubTotal()" min="0">
									</div>
									<div class="col-md-2">
										<input type="text" class="form-control rupiah" name="purchasing_price" placeholder="Harga" onkeyup="countSubTotal()" onmouseup="countSubTotal()">
									</div>
									<div class="col-md-2">
										<input type="text" class="form-control" name="purchasing_sub_total" readonly="" placeholder="Sub Total">
									</div>
									<div class="col-md-1">
										<a href="javascript: addItem()" class="btn btn-primary" ><i class="fa fa-plus"></i></a>
									</div>
								</div>
								<div class="form-group mt-30">
									<div class="col-md-10 col-md-offset-1">
										<table class="table table-hovered table-bordered table-striped">
											<thead>
												<tr>
													<th style="width: 5%">#</th>
													<th>Nama Barang</th>
													<th>Jumlah</th>
													<th>Harga</th>
													<th width="20%">Sub Total</th>
													<th style="width: 5%"><a href="javascript: deleteItem(0,0)" class="btn btn-danger"><i class="fa fa-trash"></i></a></th>
												</tr>
											</thead>
											<tbody id="result-data">
												
											</tbody>
											<tfoot>
												<tr>
													<th colspan="4" class="text-right">Total</th>
													<th colspan="2"><input type="hidden" name="purchasing_total"><span id="purchasing_total">Rp 0</span></th>
												</tr>
												<tr>
													<th colspan="4" class="text-right">Diskon</th>
													<th colspan="2">
														<div class="input-group">
															<input class="form-control" type="number" name="purchasing_discount" onkeyup="countGrandTotal()" onmouseup="countGrandTotal()">
															<span class="input-group-addon">%</span>
														</div>
													</th>
												</tr>
												<tr>
													<th colspan="4" class="text-right">Jumlah Bayar</th>
													<th colspan="2"><input type="hidden" name="purchasing_grand_total"><span id="purchasing_grand_total">Rp 0</span></th>
												</tr>
											</tfoot>
										</table>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-2 col-md-offset-7">
										<a href="javascript:goBack()" class="btn btn-warning" style="width: 100%">Kembali</a>
									</div>
									<div class="col-md-2" style="padding-left: 0px">
										<input type="submit" class="btn btn-primary" style="width: 100%" value="Simpan" >
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
	window.number = 0;
	window.grand_total = 0;
	window.arrayItem = [] ;
	$(document).ready(function() {
		$.fn.datepicker.defaults.format = "yyyy-mm-dd";
		$(".dateCustom").datepicker({ dateFormat: 'dd-mm-yy', autoclose: true, todayBtn: "linked", language: "id"});
	});
	function getMemberName(value){
		var member = $("select[name='member_id']").find(':selected').data('name');
		$("input[name='member_name']").val(member);
	}
	function getItemName(value){
		var price = $("select[name='purchasing_item']").find(':selected').data('price');
		$("input[name='purchasing_price']").val(toRp(price));
		$("input[name='jumlah']").focus();
		countSubTotal();
	}
	function countSubTotal(){
		var qty = $("input[name='purchasing_qty']").val().replace(/\D/g,'');
		var price = $("input[name='purchasing_price']").val().replace(/\D/g,'');
		var result = parseInt(qty) * parseInt(price);
		$("input[name='purchasing_sub_total']").val(toRp(result));
	}
	function addItem(){
		var item_id =  $("select[name='purchasing_item']").val();
		var item_name =  $("select[name='purchasing_item']").find(':selected').data('name');
		var qty = $("input[name='purchasing_qty']").val().replace(/\D/g,'');
		var price = $("input[name='purchasing_price']").val().replace(/\D/g,'');
		var sub_total = parseInt(qty) * parseInt(price);
		var check = true;
		$.each(arrayItem, function(key,value) {
			if(item_name ==  value){
				check = false;             
			}
		})
		if (check === false) {
			alert("Barang sudah di inputkan!");
			return false;
		} else if(qty <= 0){
			alert("Jumlah barang kosong!");
			$("input[name='jumlah']").focus();
			return false;
		} else {
			++number;
			$("#result-data").append('<tr id="detail'+number+'"><td class="text-center"><b>'+number+'.</b></td><td><input type="hidden" name="item_id[]" value="'+item_id+'"><span>'+item_name+'</span></td><td class="text-center"><input type="hidden" name="detail_qty[]" value="'+qty+'"><span>'+qty+'</span</td><td><input type="hidden" name="detail_price[]" value="'+price+'"><span>'+toRp(price)+'</span></td><td>'+toRp(sub_total)+'</td><td><a href="javascript: deleteItem('+number+','+sub_total+')" class="btn btn-danger"><i class="fa fa-times"></i></a></td></tr>');
			$( ".btn" ).addClass( "btn-flat" );
			window.grand_total += parseInt(sub_total);
			 countGrandTotal();
			arrayItem.push(item_name);
		}
		$("select[name='purchasing_item']").select2("val","0");
		$("input[name='purchasing_qty']").val("");
		$("input[name='purchasing_price']").val("");
		$("input[name='purchasing_sub_total']").val("");
	}
	function deleteItem(id, price){
		if(id == 0){
			var confirm = window.confirm("Anda yakin akan menghapus semua barang?");
			if(confirm){
				$("#result-data").html("");
				window.arrayItem = [] ;
				window.grand_total = 0;
				 countGrandTotal();
			}
		} else {
			var confirm = window.confirm("Anda yakin akan menghapus barang ini?");
			if(confirm){
				arrayItem.remove($("#detail"+id+" td span:first").text());
				$("#detail"+id).remove();
				window.grand_total -= price;
				 countGrandTotal();
			}
		}
	}
	function countGrandTotal(){
		$("#purchasing_total").text(toRp(window.grand_total));
		$("input[name='purchasing_total']").val(window.grand_total);
		var discount = $("input[name='purchasing_discount']").val();
		if(discount >0){
			var result =parseInt(window.grand_total) - (parseInt(window.grand_total) * (discount/100));
		} else {
			var result = parseInt(window.grand_total)
		}
		$("#purchasing_grand_total").text(toRp(result));
		$("input[name='purchasing_grand_total']").val(result);
	}
	Array.prototype.remove = function(x) { 
		var i;
		for(i in this){
			if(this[i].toString() == x.toString()){
				this.splice(i,1)
			}
		}
	}
</script>