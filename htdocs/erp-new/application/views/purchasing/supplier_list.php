<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Supplier</h3>
					<div class="pull-right">
						<a type="button" href="javascript:addNew()" class="btn btn-primary">Tambah Data</a>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Nomor Supplier</th>
										<th>Nama</th>
										<th>Telepon</th>
										<th>E-mail</th>
										<th>Alamat</th>
										<th style="width: 100px;">Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= $data["supplier_number"] ?></td>
										<td><?= $data["supplier_name"] ?></td>
										<td><?= $data["supplier_phone"] ?></td>
										<td><?= $data["supplier_mail"] ?></td>
										<td><?= $data["supplier_address"] ?></td>
										<td>
											<a href="javascript:update(<?= $data['supplier_id']; ?>)" id="update<?= $data['supplier_id']; ?>" data-number="<?= $data["supplier_number"] ?>" data-name="<?= $data["supplier_name"] ?>" data-address="<?= $data["supplier_address"] ?>" data-phone="<?= $data["supplier_phone"] ?>" data-mail="<?= $data["supplier_mail"] ?>" data-toggle="tooltip" data-placement="top" title="Edit <?= $data["supplier_name"] ?>" class="btn btn-sm btn-warning btn-flat"><i class="fa fa-edit"></i></a>
											<a href="<?= base_url()?>panel/purchasing/listSupplier/delete/<?= $data['supplier_id']; ?>" onclick="return confirm('Yakin akan menghapus data <?= $data["supplier_name"] ?>?')" data-toggle="tooltip" data-placement="top" title="Hapus <?= $data["supplier_name"] ?>" class="btn btn-sm btn-danger btn-flat"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Input Data Supplier</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="<?= base_url()?>panel/purchasing/listSupplier/save"  method="post">
					<div class="form-group">
						<label class="col-md-3 control-label">Nomor Supplier</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="supplier_number" placeholder="Nomor Supplier" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Nama</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="supplier_name" placeholder="Nama" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Telepon</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="supplier_phone" placeholder="Telepon" required/>								
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">E-mail</label>
						<div class="col-md-7">
							<input type="email" class="form-control" name="supplier_mail" placeholder="E-mail" required/>							
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">Alamat</label>
						<div class="col-md-7">
							<textarea class="form-control" name="supplier_address" placeholder="Alamat" required></textarea>							
						</div>
					</div>
					<input type="hidden" name="id" id="id" />
				</div>
				<div class="modal-footer">
					<a data-dismiss="modal" class="btn btn-warning btn-flat">Batal</a>
					<button type="submit" id="submit-form" class="btn btn-primary btn-flat">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#formModal').on('shown.bs.modal', function () {
			$("input[name='supplier_number']").focus();
		})  
	});
	function addNew(){
		$("input[name='supplier_number']").val("");
		$("input[name='supplier_name']").val("");
		$("input[name='supplier_phone']").val("");
		$("input[name='supplier_mail']").val("");
		$("textarea[name='supplier_address']").val("");
		$("#myModalLabel").text("Input Data Supplier");
		$("#id").val(0);
		$("#submit-form").text("Simpan");
		$("#formModal").modal("show");
	}
	function update(id){
		$("input[name='supplier_number']").val($("#update"+id).data("number"));
		$("input[name='supplier_name']").val($("#update"+id).data("name"));
		$("input[name='supplier_phone']").val($("#update"+id).data("phone"));
		$("input[name='supplier_mail']").val($("#update"+id).data("mail"));
		$("textarea[name='supplier_address']").val($("#update"+id).data("address"));
		$("#myModalLabel").text("Edit Data Supplier");
		$("#id").val(id);
		$("#submit-form").text("Edit");
		$("#formModal").modal("show");
	}
</script>