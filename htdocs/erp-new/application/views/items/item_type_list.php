<section class="content">
	<div class="row">
		<div class="col-lg-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Data Jenis Barang</h3>
					<div class="pull-right">
						<a type="button" href="javascript:addNew()" class="btn btn-primary">Tambah Data</a>
					</div>
				</div>
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
							<?=$this->session->userdata('message_action') ?>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 table-responsive">
							<table class="table table-striped table-bordered table-hover" id="generalTable">
								<thead>
									<tr>
										<th>#</th>
										<th>Jenis</th>
										<th style="width: 100px;">Opsi</th>
									</tr>
								</thead>
								<tbody>
									<?php $start = 0; foreach ($data_array as $data) { ?>
									<tr>
										<td><?= ++$start ?>.</td>
										<td><?= $data["type_name"] ?></td>
										<td>
											<a href="javascript:update(<?= $data['type_id']; ?>)" id="update<?= $data['type_id']; ?>" data-name="<?= $data["type_name"] ?>" data-toggle="tooltip" data-placement="top" title="Edit <?= $data["type_name"] ?>" class="btn btn-sm btn-warning btn-flat"><i class="fa fa-edit"></i></a>
											<a href="<?= base_url()?>panel/items/listType/delete/<?= $data['type_id']; ?>" onclick="return confirm('Yakin akan menghapus data <?= $data["type_name"] ?>?')" data-toggle="tooltip" data-placement="top" title="Hapus <?= $data["type_name"] ?>" class="btn btn-sm btn-danger btn-flat"><i class="fa fa-trash-o"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="formModal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">Input Data Jenis Barang</h4>
			</div>
			<div class="modal-body">
				<form class="form-horizontal" action="<?= base_url()?>panel/items/listType/save"  method="post">
					<div class="form-group">
						<label class="col-md-3 control-label">Jenis Barang</label>
						<div class="col-md-7">
							<input type="text" class="form-control" name="type_name" placeholder="Jenis Barang" required/>								
						</div>
					</div>
					<input type="hidden" name="id" id="id" />
				</div>
				<div class="modal-footer">
					<a data-dismiss="modal" class="btn btn-warning btn-flat">Batal</a>
					<button type="submit" id="submit-form" class="btn btn-primary btn-flat">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('#formModal').on('shown.bs.modal', function () {
			$("input[name='type_name']").focus();
		})  
	});
	function addNew(){
		$("input[name='type_name']").val("");
		$("#myModalLabel").text("Input Data Jenis Barang");
		$("#id").val(0);
		$("#submit-form").text("Simpan");
		$("#formModal").modal("show");
	}
	function update(id){
		$("input[name='type_name']").val($("#update"+id).data("name"));
		$("#myModalLabel").text("Edit Data Jenis Barang");
		$("#id").val(id);
		$("#submit-form").text("Edit");
		$("#formModal").modal("show");
	}
</script>