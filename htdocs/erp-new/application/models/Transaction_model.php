<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Transaction_model extends CI_Model {

	function getAllCash()
	{
		$this->db->distinct();
		$this->db->select("a.transaction_id, a.transaction_number, a.transaction_date, a.transaction_total, a.transaction_discount, a.transaction_grand_total, b.member_name");
		$this->db->from("transaction a");
		$this->db->join("member b","a.member_id = b.member_id");
		$this->db->where("a.transaction_type",1);
		$query = $this->db->get()->result_array();
		return $query;
	}
	function getDetailCash($id)
	{
		$this->db->distinct();
		$this->db->select("a.detail_qty, a.detail_price, (a.detail_qty * a.detail_price) as sub_total, b.item_name");
		$this->db->from("transaction_detail a");
		$this->db->join("item b","a.item_id = b.item_id");
		$this->db->where("a.transaction_id",$id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	function getAllCredit()
	{
		$this->db->distinct();
		$this->db->select("a.transaction_id, a.transaction_number, a.transaction_date, a.transaction_due_date,  a.transaction_total, a.transaction_discount, a.transaction_grand_total, b.member_name");
		$this->db->from("transaction a");
		$this->db->join("member b","a.member_id = b.member_id");
		$this->db->where("a.transaction_type",2);
		$query = $this->db->get()->result_array();
		return $query;
	}
	function getDetailCredit($id)
	{
		$this->db->distinct();
		$this->db->select("a.detail_qty, a.detail_price, (a.detail_qty * a.detail_price) as sub_total, b.item_name");
		$this->db->from("transaction_detail a");
		$this->db->join("item b","a.item_id = b.item_id");
		$this->db->where("a.transaction_id",$id);
		$query = $this->db->get()->result_array();
		return $query;
	}
	function getAllCreditReceive()
	{
		$this->db->select("a.transaction_id, a.transaction_number, a.transaction_date, a.transaction_due_date, a.transaction_total, a.transaction_discount, a.transaction_grand_total, b.member_name, b.member_id, SUM(c.recieve_amount) as has_receive");
		$this->db->from("transaction a");
		$this->db->join("member b","a.member_id = b.member_id");
		$this->db->join("transaction_credit_recieve c","a.transaction_id = c.transaction_id","LEFT");
		$this->db->where("a.transaction_type",2);
		$this->db->group_by("a.transaction_id");
		$query = $this->db->get()->result_array();
		return $query;
	}
	function getDetailCreditReceive($id)
	{
		$this->db->distinct();
		$this->db->select("a.recieve_amount, a.recieve_type, a.recieve_create_date");
		$this->db->from("transaction_credit_recieve a");
		$this->db->where("a.transaction_id",$id);
		$query = $this->db->get()->result_array();
		return $query;
	}	

}
/* End of file Transaction_model.php */
/* Location: ./application/models/Transaction_model.php */