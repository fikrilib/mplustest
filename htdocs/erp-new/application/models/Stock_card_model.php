<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_card_model extends CI_Model {

	function getData($filter)
	{
		$this->db->select("*");
		$this->db->from("stok_mutasi");
		$this->db->where("item_id",$filter['item_id']);
		$this->db->where("DATE_FORMAT(date,'%Y-%m-%d') >=",$filter['start_date']); 
		$this->db->where("DATE_FORMAT(date,'%Y-%m-%d') <=",$filter['end_date']); 
		$this->db->order_by("id","asc");
		$query = $this->db->get();
		return $query->result_array();
	}
	function getlastRecord($id)
	{
		$this->db->select("balance_stock_qty, balance_stock_price, balance_stock_total");
		$this->db->from("stok_mutasi");
		$this->db->where("item_id",$id);
		$this->db->order_by("id","desc");
		$this->db->limit(1);
		$query = $this->db->get();
		return $query->row_array();
	}

}

/* End of file Stock_card_model.php */
/* Location: ./application/models/Stock_card_model.php */