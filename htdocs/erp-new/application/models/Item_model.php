<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Item_model extends CI_Model {

	function getAll()
	{
		$this->db->distinct();
		$this->db->select("a.item_id, a.type_id, a.item_number, a.item_name, a.item_stock, a.item_purchase_price, a.item_sold_price, b.type_name");
		$this->db->from("item a");
		$this->db->join("item_type b","a.type_id = b.type_id");
		$query = $this->db->get()->result_array();
		return $query;
	}

}

/* End of file Item_model.php */
/* Location: ./application/models/Item_model.php */