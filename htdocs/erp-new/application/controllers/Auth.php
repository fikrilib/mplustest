<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->library('Encrypter');
	}
	public function index()
	{
		if(empty($this->session->userdata('user'))){
			$this->load->view('templates/login');
		}else{
			echo "<script>window.location.href='" . base_url() . "panel/dashboard';</script>";
		}
	}
	public function login()
	{
		if($this->input->post('login')!="yes")
		{
			$this->load->view('templates/login');
		}else
		{
			$data=array(
				"username" => $this->security->xss_clean($this->input->post("username")),
				"password" =>  $this->encrypter->encryptIt($this->security->xss_clean($this->input->post("password"))),
				);

			$query=$this->Db_umum->cek_login($data);
			$row=$query->num_rows();
			if($row==1)
			{					
				foreach ($query->result() as $row)
				{	
					$newdata = array(
						'user'  => $row->username,
						'pas'     => $row->password,
						'jenis' => $row->jenis,
						'uid' => $row->id,
						'status' => TRUE
						);
					$this->session->set_userdata($newdata);
				}
				?>
				<script>							
					document.location="<?php echo base_url().'User'; ?>";
				</script>
				<?php
			} else {
				?>
				<script>
					alert("Kombinasi Password dan username anda tidak benar silahkan login lagi");
					document.location="<?php echo base_url()."Auth/login"; ?>";
				</script>
				<?php								
			}
		}
	}

	public function logout()
	{
		$newdata = array(
			'user'  => "",
			'pas'     => "",
			'status' => "",
			'jenis' => ""
			);
		$this->session->unset_userdata($newdata);
		$this->session->sess_destroy();
		$url=base_url()."Auth";
		warning_massage("Berhasil Logout",$url);
	}
}
