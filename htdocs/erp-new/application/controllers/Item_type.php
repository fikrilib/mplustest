<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Item_type extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$data['data_array'] = $this->Db_umum->getSelect("type_id, type_name","item_type")->result_array();

		show("items/item_type_list", "Jenis Barang", $data);
	}
	function save()
	{
		$id = $this->input->post("id");
		if($id == 0){
			$data = array(
				"type_name" => $this->input->post("type_name"),
				);
			$this->Db_umum->insert("item_type",$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input data berhasil</div>');
		} else {
			$data = array(
				"type_name" => $this->input->post("type_name"),
				);
			$this->Db_umum->update("item_type","type_id",$id,$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Edit data berhasil</div>');
		}
		redirect('panel/items/listType','refresh');
	}
	function delete($id)
	{
		$this->Db_umum->delete("item_type","type_id", $id);
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Hapus data berhasil</div>');
		redirect('panel/items/listType','refresh');
	}
}

/* End of file Item_type.php */
/* Location: ./application/controllers/Item_type.php */