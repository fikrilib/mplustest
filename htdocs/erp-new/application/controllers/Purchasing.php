<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Purchasing extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Purchasing_model');
		$this->load->model('Stock_card_model');
	}
	public function form()
	{
		$q_No = $this->Db_umum->row("SELECT purchasing_id FROM purchasing");
		$data['purchasing_number'] = 'PB-'.$q_No;
		$data['data_supplier'] = $this->Db_umum->getSelect("supplier_id, supplier_name","supplier")->result_array();
		$data['data_member'] = $this->Db_umum->getSelect("member_id, member_number, member_name","member")->result_array();
		$data['data_item'] = $this->Db_umum->getSelect("item_id, item_number, item_name, item_purchase_price","item")->result_array();

		show("purchasing/purchasing_form", "Input Pembelian", $data);
	}
	public function list()
	{
		$data['data_array'] = $this->Purchasing_model->getAll();

		show("purchasing/purchasing_list", "Riwayat Pembelian", $data);
	}
	function save()
	{
		$data = array(
			"member_id" => $this->input->post("member_id"),
			"supplier_id" => $this->input->post("supplier_id"),
			"purchasing_number" => $this->input->post("purchasing_number"),
			"purchasing_date" => $this->input->post("purchasing_date"),
			"purchasing_total" => $this->input->post("purchasing_total"),
			"purchasing_discount" => $this->input->post("purchasing_discount"),
			"purchasing_grand_total" => $this->input->post("purchasing_grand_total"),
			"purchasing_create_date" => date("Y-m-d H:i:s"),
			"purchasing_state" => 1,
			);
		$purchasing_id = $this->Db_umum->insert("purchasing",$data);

		$detail_id = $this->input->post("item_id",TRUE);
		
		$count =  count($detail_id);
		for($i=0; $i<$count; $i++){
			$data = array(
				"item_id" => $_POST['item_id'][$i],
				"price" => $_POST['detail_price'][$i],
				);
			$this->Db_umum->insert("item_calculation",$data);

			// $getCalculation = $this->Db_umum->getById("price","item_calculation","item_id",$_POST['item_id'][$i])->result_array();
			// $calculationPrice = 0;
			// $divide = count($getCalculation);
			// foreach ($getCalculation as $calculation) {
			// 	$calculationPrice += $calculation['price'];
			// }
			// $finalPrice = $calculationPrice / $divide;


			$item = $this->Db_umum->getById("item_stock","item","item_id",$_POST['item_id'][$i])->row_array();
			$final_stock = $item['item_stock'] + $_POST['detail_qty'][$i];
			$detail_price =  $_POST['detail_price'][$i];
			$data = array(
				"purchasing_id" => $purchasing_id,
				"item_id" => $_POST['item_id'][$i],
				"detail_qty" => $_POST['detail_qty'][$i],
				"detail_price" => $detail_price,
				);
			$this->Db_umum->insert("purchasing_detail",$data);

			$last_mutation = $this->Stock_card_model->getlastRecord($_POST['item_id'][$i]);
			$stock_total = $detail_price * $_POST['detail_qty'][$i]; 
			$balance_stock_qty = $last_mutation['balance_stock_qty'] + $_POST['detail_qty'][$i];
			$balance_stock_price = ($last_mutation['balance_stock_total'] + $stock_total ) / $balance_stock_qty;
			$balance_stock_total = $last_mutation['balance_stock_total'] + $stock_total;
			$data2 = array(
				"item_id" => $_POST['item_id'][$i],
				"id_order" => $this->input->post("purchasing_number"),
				"stock_qty" => $_POST['detail_qty'][$i],
				"stock_price" => $detail_price,
				"stock_total" => $stock_total,
				"balance_stock_qty" => $balance_stock_qty,
				"balance_stock_price" => $balance_stock_price,
				"balance_stock_total" => $balance_stock_total,
				"description" => "Pembelian",
				"stock_type" => 2,
				"date" =>date("Y-m-d H:i:s"),
				);
			$this->Db_umum->insert("stok_mutasi",$data2);
			$data = array(
				"item_stock" => $final_stock,
				"item_purchase_price" => $detail_price,
				// "item_sold_price" => $balance_stock_price,
				);
			$this->Db_umum->update("item","item_id",$_POST['item_id'][$i],$data);

		}
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input Pembelian berhasil</div>');
		redirect('panel/purchasing/formPurchasing','refresh');
	}
	function detail($id)
	{
		$array_data = $this->Purchasing_model->getDetail($id);
		$jason['detail_purchasing'] = array();
		$number = 0;
		foreach ($array_data as $data) {
			$item['number'] = ++$number;
			$item['name'] = $data['item_name'];
			$item['qty'] = $data['detail_qty'];
			$item['price'] = $data['detail_price'];
			$item['sub_total'] = $data['sub_total'];
			array_push($jason['detail_purchasing'],$item);
		}
		echo json_encode($jason);
	}
}

/* End of file Purchasing.php */
/* Location: ./application/controllers/Purchasing.php */