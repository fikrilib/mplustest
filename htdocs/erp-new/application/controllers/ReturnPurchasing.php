<?php defined('BASEPATH') OR exit('No direct script access allowed');

class ReturnPurchasing extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Return_model');
	}
	public function index()
	{
		$data['data_array'] = $this->Return_model->getAllPurchasing();

		show("retur/purchasing_list", "Retur Pembelian", $data);
	}
	public function create()
	{
		$data['data_item'] = $this->Db_umum->getSelect("item_id, item_number, item_name, item_sold_price, item_stock","item")->result_array();

		show("retur/purchasing_form", "Input Retur Pembelian", $data);
	}
	function save()
	{
		$data = array(
			"return_description" => $this->input->post("return_description"),
			"return_create_date" => date("Y-m-d H:i:s"),
			"return_type" => 1,
			"return_state" => 1,
			);
		$return_id = $this->Db_umum->insert("return",$data);

		$detail_id = $this->input->post("item_id",TRUE);
		
		$count =  count($detail_id);
		for($i=0; $i<$count; $i++){
			$data = array(
				"return_id" => $return_id,
				"item_id" => $_POST['item_id'][$i],
				"detail_quantity" => $_POST['detail_qty'][$i],
				);
			$this->Db_umum->insert("return_detail",$data);

			$item = $this->Db_umum->getById("item_stock","item","item_id",$_POST['item_id'][$i])->row_array();
			$final_stock = $item['item_stock'] - $_POST['detail_qty'][$i];
			$data = array(
				"item_stock" => $final_stock,
				);
			$this->Db_umum->update("item","item_id",$_POST['item_id'][$i],$data);

		}
		redirect('panel/returnItem/listReturnPurchasing','refresh');
	}
	function detail($id)
	{
		$array_data = $this->Return_model->getDetail($id);

		echo json_encode($array_data);
	}

}

/* End of file ReturnPurchasin.php */
/* Location: ./application/controllers/ReturnPurchasin.php */