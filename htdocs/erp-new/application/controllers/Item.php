<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Item extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Item_model');
	}
	public function index()
	{
		$data['data_array'] = $this->Item_model->getAll();
		$data['data_type'] = $this->Db_umum->getSelect("type_id, type_name","item_type")->result_array();

		show("items/item_list", "Barang", $data);
	}
	function save()
	{
		$id = $this->input->post("id");
		if($id == 0){
			$data = array(
				"type_id" => $this->input->post("type_id"),
				"item_number" => $this->input->post("item_number"),
				"item_name" => $this->input->post("item_name"),
				"item_stock" => $this->input->post("item_stock"),
				"item_purchase_price" => preg_replace("/\D/", '',$this->input->post("item_purchase_price")),
				"item_sold_price" => preg_replace("/\D/", '',$this->input->post("item_sold_price")),
				"item_create_date" => date("Y-m-d H:i:s"),
				"item_state" => 1
				);
			$item_id = $this->Db_umum->insert("item",$data);
			$data2 = array(
				"item_id" => $item_id,
				"id_order" => "",
				"stock_qty" =>"",
				"stock_price" => "",
				"stock_total" => "",
				"balance_stock_qty" => $data['item_stock'],
				"balance_stock_price" => $data['item_purchase_price'],
				"balance_stock_total" => $data['item_stock'] * $data['item_purchase_price'],
				"description" => "Saldo Awal",
				"stock_type" => 1,
				"date" =>date("Y-m-d H:i:s"),
				);
			$this->Db_umum->insert("stok_mutasi",$data2);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Input data berhasil</div>');
		} else {
			$data = array(
				"type_id" => $this->input->post("type_id"),
				"item_number" => $this->input->post("item_number"),
				"item_name" => $this->input->post("item_name"),
				"item_stock" => $this->input->post("item_stock"),
				"item_purchase_price" => $this->input->post("item_purchase_price"),
				"item_purchase_price" => preg_replace("/\D/", '',$this->input->post("item_purchase_price")),
				"item_sold_price" => preg_replace("/\D/", '',$this->input->post("item_sold_price")),
				"item_last_update" => date("Y-m-d H:i:s")
				);
			$this->Db_umum->update("item","item_id",$id,$data);
			$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Edit data berhasil</div>');
		}
		redirect('panel/items/listItem','refresh');
	}
	function delete($id)
	{
		$this->Db_umum->delete("item","item_id", $id);
		$this->session->set_flashdata('message_action', '<div class="alert alert-success" role="alert">  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Hapus data berhasil</div>');
		redirect('panel/items/listItem','refresh');
	}
}

/* End of file Item.php */
/* Location: ./application/controllers/Item.php */