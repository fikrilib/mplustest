<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Stock_card extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Stock_card_model');
	}
	public function index()
	{
		$data['array_goods'] = $this->Db_umum->getSelect("item_id,item_name","item")->result_array();
		show("stock_card/index", "Kartu Persediaan", $data);
	}
	public function loadData()
	{
		$filterArray = array(
			"start_date" => $this->input->post('start_date'),
			"end_date" => $this->input->post('end_date'),
			"item_id" => $this->input->post('item_id'),
			);
		$report = $this->Stock_card_model->getData($filterArray);
		$start = 0;
		$prefix = '{ "data" : ';
		if( ! empty($report)){
			$start = 0;
			foreach ($report as $data){ 
				if($data['stock_type'] == 1){
					$qty1 = "-";
					$price1 = "-";
					$total1 = "-";
					$qty2 = "-";
					$price2 = "-";
					$total2 = "-";
					$qty3 = number_format($data['balance_stock_qty'],0, ",",".");
					$price3 = number_format($data['balance_stock_price'],0, ",",".");
					$total3 = number_format($data['balance_stock_total'],0, ",",".");
				} else if ($data['stock_type'] == 2){
					$qty1 = $data['stock_qty'];
					$price1 = number_format($data['stock_price'],0, ",",".");
					$total1 = number_format($data['stock_total'],0, ",",".");
					$qty2 = "-";
					$price2 = "-";
					$total2 = "-";
					$qty3 = $data['balance_stock_qty'];
					$price3 = number_format($data['balance_stock_price'],0, ",",".");
					$total3 = number_format($data['balance_stock_total'],0, ",",".");
				} else {
					$qty1 = "-";
					$price1 = "-";
					$total1 = "-";
					$qty2 = $data['stock_qty'];
					$price2 = number_format($data['stock_price'],0, ",",".");
					$total2 = number_format($data['stock_total'],0, ",",".");
					$qty3 = number_format($data['balance_stock_qty'],0, ",",".");
					$price3 = number_format($data['balance_stock_price'],0, ",",".");
					$total3 = number_format($data['balance_stock_total'],0, ",",".");
				}
				$listData[] = array(
					indonesianDate($data['date']),
					$data['description'],
					$data['id_order'],
					$qty1,
					$price1,
					$total1,
					$qty2,
					$price2,
					$total2,
					$qty3,
					$price3,
					$total3,
					);
			}
			echo $prefix.json_encode($listData, JSON_PRETTY_PRINT).'}';
		} else {
			echo '{ 	
				"sEcho": 1,
				"iTotalRecords": "0",
				"iTotalDisplayRecords": "0",
				"aaData": []
			}';
		}
	}
}

/* End of file Stock_card.php */
/* Location: ./application/controllers/Stock_card.php */