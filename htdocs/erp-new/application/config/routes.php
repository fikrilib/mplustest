<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route["panel"] = "Auth";
$route["panel/login"] = "Auth";
$route["panel/login/auth"] = "Auth/login";
$route["panel/logout"] = "Auth/logout";

//dashboard
$route["panel/dashboard"] = "Dashboard";

//user
$route["panel/user/listUser"] = "User";
$route["panel/user/listUser/save"] = "User/save";
$route["panel/user/listUser/delete/(:num)"] = "User/delete/$1";

//member
$route["panel/member/listMember"] = "Member";
$route["panel/member/listMember/save"] = "Member/save";
$route["panel/member/listMember/delete/(:num)"] = "Member/delete/$1";

//item type
$route["panel/items/listType"] = "Item_type";
$route["panel/items/listType/save"] = "Item_type/save";
$route["panel/items/listType/delete/(:num)"] = "Item_type/delete/$1";

//item
$route["panel/items/listItem"] = "Item";
$route["panel/items/listItem/save"] = "Item/save";
$route["panel/items/listItem/delete/(:num)"] = "Item/delete/$1";

$route["panel/items/stockCard"] = "Stock_card";
$route["panel/items/stockCard/load"] = "Stock_card/loadData";

//supplier
$route["panel/purchasing/listSupplier"] = "Supplier";
$route["panel/purchasing/listSupplier/save"] = "Supplier/save";
$route["panel/purchasing/listSupplier/delete/(:num)"] = "Supplier/delete/$1";

//purchasing form
$route["panel/purchasing/formPurchasing"] = "Purchasing/form";
$route["panel/purchasing/formPurchasing/save"] = "Purchasing/save";

//purchasing list
$route["panel/purchasing/listPurchasing"] = "Purchasing/list";
$route["panel/purchasing/listPurchasing/detail/(:num)"] = "Purchasing/detail/$1";

//cash form
$route["panel/transaction/cash/formCash"] = "TransactionCash/form";
$route["panel/transaction/cash/formCash/save"] = "TransactionCash/save";

$route["panel/transaction/cash/barcode"] = "TransactionCash/barcodeForm";
$route["panel/transaction/cash/barcode/save"] = "TransactionCash/barcodeSave";

//cash list
$route["panel/transaction/cash/listCash"] = "TransactionCash/list";
$route["panel/transaction/cash/listCash/detail/(:num)"] = "TransactionCash/detail/$1";

//credit form
$route["panel/transaction/credit/formCredit"] = "TransactionCredit/form";
$route["panel/transaction/credit/formCredit/save"] = "TransactionCredit/save";

//credit list
$route["panel/transaction/credit/listCredit"] = "TransactionCredit/list";
$route["panel/transaction/credit/listCredit/detail/(:num)"] = "TransactionCredit/detail/$1";

//recieve list
$route["panel/receivable/listReceivable"] = "TransactionCredit/listRecieve";
$route["panel/receivable/listReceivable/detail/(:num)"] = "TransactionCredit/detailReceive/$1";
$route["panel/receivable/listReceivable/save"] = "TransactionCredit/saveRecieve";
$route["panel/receivable/receivableCard"] = "TransactionCredit/receivableCard";

//retur purchasing
$route["panel/returnItem/listReturnPurchasing"] = "ReturnPurchasing";
$route["panel/returnItem/listReturnPurchasing/detail/(:num)"] = "ReturnPurchasing/detail/$1";
$route["panel/returnItem/listReturnPurchasing/create"] = "ReturnPurchasing/create";
$route["panel/returnItem/listReturnPurchasing/save"] = "ReturnPurchasing/save";

//retur transaction
$route["panel/returnItem/listReturnTransaction"] = "ReturnTransaction";
$route["panel/returnItem/listReturnTransaction/detail/(:num)"] = "ReturnTransaction/detail/$1";
$route["panel/returnItem/listReturnTransaction/create"] = "ReturnTransaction/create";
$route["panel/returnItem/listReturnTransaction/save"] = "ReturnTransaction/save";

//report
$route["panel/report/purchasing"] = "Report/transaction";
$route["panel/report/purchasing/print/(:num)/(:num)"] = "Report/printTransaction";
$route["panel/report/transactionCash"] = "Report/cash";
$route["panel/report/transactionCash/print/(:num)/(:num)"] = "Report/printCash";
$route["panel/report/transactionCredit"] = "Report/credit";
$route["panel/report/transactionCredit/print/(:num)/(:num)"] = "Report/printCredit";
$route["panel/report/stock"] = "Report/persediaan";
$route["panel/report/profitLoss"] = "Report/labarugi";
$route["panel/report/profitLoss/print/(:num)/(:num)"] = "Report/printLabaRugi";


