<?php
if ( ! function_exists('show'))
{
	function namabulan_1($bln){
		if ($bln==1) $bil='Jan';
		else if ($bln==2) $bil='Feb';
		else if ($bln==3) $bil='Mar';
		else if ($bln==4) $bil='Apr';
		else if ($bln==5) $bil='Mei';
		else if ($bln==6) $bil='Jun';
		else if ($bln==7) $bil='Jul';
		else if ($bln==8) $bil='Agu';
		else if ($bln==9) $bil='Sep';
		else if ($bln==10) $bil='Okt';
		else if ($bln==11) $bil='Nov';
		else if ($bln==12) $bil='Des';
		return $bil;
	}

	function namabulan_2($bln){
		if ($bln==1) $bil='Januari';
		else if ($bln==2) $bil='Februari';
		else if ($bln==3) $bil='Maret';
		else if ($bln==4) $bil='April';
		else if ($bln==5) $bil='Mei';
		else if ($bln==6) $bil='Juni';
		else if ($bln==7) $bil='Juli';
		else if ($bln==8) $bil='Agustus';
		else if ($bln==9) $bil='September';
		else if ($bln==10) $bil='Oktober';
		else if ($bln==11) $bil='November';
		else if ($bln==12) $bil='Desember';
		return $bil;
	}
	
	function show($view,$title = "", $data)
	{
		global $template;
		$ci = &get_instance();
		$data['page_title'] = $title." | ";
		$data['view'] = $view;
		$ci->load->view('templates/'.$template.'/template_page_view', $data);
	}
	function indonesianDate($date,$type = 1){
		$monthArray = array( '01' => 'Januari', '02' => 'Februari', '03' => 'Maret', '04' => 'April', '05' => 'Mei', '06' => 'Juni', '07' => 'Juli', '08' => 'Agustus', '09' => 'September', '10' => 'Oktober', '11' => 'November', '12' => 'Desember', );
		$dayArray = array( '1' => 'Senin',  '2' => 'Selasa',  '3' => 'Rabu',  '4' => 'Kamis',  '5' => 'Jumat',  '6' => 'Sabtu', '7' => 'Minggu',);
		if(isset($date)){
			$date = date_create($date);
			if($type == 1){
				return date_format($date,"d")." ".$monthArray[date_format($date,"m")]." ".date_format($date,"Y");
			} else if($type ==2){
				return $monthArray[date_format($date,"m")];
			} else if($type ==3){
				return $monthArray[date_format($date,"m")]." ".date_format($date,"Y");
			}
		} else {
			return "-";
		}
	}

	function filter_bulan()
	{
		if(isset($_POST['bulan']))
		{
			$bulan=$_POST['bulan'];
		}else
		{
			$bulan="";
		}

		$bln=array("Januari","Februari","Maret","April","Mei","Juni","Juli",
			"Agustus","September","Oktober","November","Desember");
		echo "<td><select name='bulan' class='form-control'><option value=''>All</option>";
		$b=0;
		for($a=1;$a<=count($bln);$a++)
		{
			if($a==$bulan)
			{
				$s='selected';
			}else
			{
				$s='';
			}
			echo '<option value="'.$a.'" '.$s.'>'.$bln[$b].'</option>';
			$b++;
		}
		echo '</select></td>';

	}

	function filter_tahun()
	{
		if(isset($_POST['tahun']))
		{
			$t=$_POST['tahun'];
		}else
		{
			$t=date('Y');
		}
		$dt=date("Y");
		echo "<td><select name='tahun' class='form-control'>";
		for($a=0;$a<5;$a++)
		{
			if($t==$dt)
			{
				$s="selected";
			}else
			{
				$s="";
			}
			echo "<option value='".$dt."' ".$s.">".$dt."</option>";
			$dt--;
		}
		echo "</select></td>";
	}

	function filter_tahun2()
	{
		if(isset($_POST['tahun']))
		{
			$t=$_POST['tahun'];
		}else
		{
			$t=date('Y');
		}
		$dt=date("Y");
		echo "<select id='tahun' name='tahun' class='form-control'>";
		for($a=0;$a<5;$a++)
		{
			if($t==$dt)
			{
				$s="selected";
			}else
			{
				$s="";
			}
			echo "<option value='".$dt."' ".$s.">".$dt."</option>";
			$dt--;
		}
		echo "</select>";
	}

	function lembaga()
	{
		return strtoupper("SiJaPil");
	}

	function writeRow($val) {
		
		echo '<td>'.utf8_decode($val).'</td>';              
	}

	function warning_massage($massage,$url)
	{
		?>
		<script>
			alert("<?php echo $massage ?>");
			document.location="<?php echo $url; ?>";
		</script>
		<?php
	}

	function  show_pop_up($view, $data)
	{
		global $template;
		$ci = &get_instance();
		$data['view'] = $view;
		$ci->load->view('templates/'.$template.'/template_pop_up', $data);
	}

	function form_aku($label,$type,$name,$val,$validasi,$field=9,$placeholder = NULL)
	{
		$hasil1= '<div class="form-group row">
		<label for="'.$name.'" class="col-sm-3 label-control">'.$label.'</label>
		<div class="col-sm-'.$field.'">	';													
			$hasil2= '</div>
		</div>';

		if($type!="hidden")
		{
			if($type=='date' OR $type=='date1')
			{
				$hasil3='<input type="text" id="'.$type.'" name="'.$name.'" '.$validasi.' value="'.$val.'" class="form-control datepicker-default">';
			}else 
			if($type=='text' || $type=='password')
			{
				$hasil3='<input type="'.$type.'" id="'.$name.'" name="'.$name.'" '.$validasi.' value="'.$val.'" class="form-control" placeholder="'.$placeholder.'">';
			}else
			if($type=='textarea')
			{
				$hasil3='<'.$type.' id="'.$name.'" name="'.$name.'" '.$validasi.' rows="2" class="form-control">'.$val.'</'.$type.'>';
			}else
			if($type=='radio')
			{
				$hasil3="";
				$valid="";
				foreach($val as $val)
				{
					if($val['label']=="checkedyes")
					{
						$valid=$val['valu'];
					}else
					{
						if($valid==$val['valu'])
							$sel='checked';
						else
							$sel='';

						if($hasil3=="")
						{
							$hasil3.='<input type="'.$type.'" id="'.$name.'" name="'.$name.'" '.$validasi.' '.$sel.' value="'.$val['valu'].'" class=""> '.$val['label'].' ';
						}else
						{
							$hasil3.='<input type="'.$type.'" id="'.$name.'" name="'.$name.'" '.$sel.' '.$validasi.'  value="'.$val['valu'].'" class=""> '.$val['label'].' ';
						}
					}
				}
			}else
			if($type=='select')
			{
				$hasil3='<Select  id="'.$name.'" name="'.$name.'" class="form-control select2" style="width:100%;">';
				$dat="";
				foreach($val as $val)
				{
					if($val['label']!='selected')
					{
						if($dat==$val['value'])
							$sel='selected';
						else
							$sel='';

						if($hasil3=='<Select name="'.$name.'" class="form-control">')
						{
							$hasil3.='<option value="'.$val['value'].'" '.$sel.'>'.$val['label'].'</option>';
						}else
						{
							$hasil3.='<option value="'.$val['value'].'" '.$sel.'>'.$val['label'].'</option>';
						}
					}else
					{
						$dat=$val['value'];
					}
				}
				$hasil3.="</select>";
			}
			$tampil=$hasil1.$hasil3.$hasil2;
		}else
		{
			$hasil3='<input type="'.$type.'" id="'.$name.'" name="'.$name.'" '.$validasi.' value="'.$val.'" class="form-control">';
			$tampil=$hasil3	;
		}
		echo $tampil;
	}

	function tampil_menu_semua()
	{
		echo menu_semua(0,$h="",0);
	}

	function menu($parent=0,$hasil,$sub,$lev1)
	{
		if(!empty($lev1))
		{
			$lev1=array_unique($lev1);
			if($parent==0)
			{
				$hasil .= "<ul class='sidebar-menu'>";
			}else
			{
				$hasil .= "<ul class='treeview-menu'>";
			}


			foreach($lev1 as $h)
			{
				if(!empty($sub[$h]))
				{
					$icont=icon($h);
					$hasil .='<li class="treeview">
					<a href="#">
						<i class="fa '.$icont.'"></i>
						<span>'.$h.'</span>
						<i class="fa fa-angle-left pull-right"></i>
					</a>';
				}else
				{
					$link=cari_link_salah($h,$parent);
					$link=base_url().$link;
					$hasil .='  <li><a href="'.$link.'"><i class="fa fa-file-text-o"></i>'.$h.'</a>';
				}
				if(!empty($sub[$h]))
				{
					$par=search_parent($h);
					$hasil = menu($par,$hasil,$sub,$sub[$h]);
				}
				$hasil .= "</li>";
			}

			if(!empty($lev1))
			{
				$hasil .= "</ul>";
			}
		}
		return $hasil;	
	}

	function menu_semua($parent=0,$hasil)
	{
		$ci = &get_instance();
		$w = $ci->db->query("SELECT * from menu where parent='".$parent."'");

		if($parent==0)
		{
			$hasil .= "<ul class='sidebar-menu'>";
		}else
		{
			$hasil .= "<ul class='treeview-menu'>";
		}
		foreach($w->result() as $h)
		{
			$x=$ci->db->query("SELECT * from menu where parent='".$h->id_menu."'");
			if(($x->num_rows)>0)
			{
				$icont=icon($h->nama);
				$hasil .='<li class="treeview">
				<a href="#">
					<i class="fa '.$icont.'"></i>
					<span>'.$h->nama.'</span>
					<i class="fa fa-angle-left pull-right"></i>
				</a>';
			}else
			{
				$link=cari_link($h->id_menu);
				$link=base_url().$link;
				$hasil .='<li><a href="'.$link.'"><i class="fa fa-file-text-o"></i>'.$h->nama.'</a>';
			}	
			$hasil = menu_semua($h->id_menu,$hasil);

			$hasil .= "</li>";
		}
		$hasil .= "</ul>";

		return $hasil;
	}

	function setting_menu($user,$pas)
	{
		$ci=& get_instance();
			// $sql = "select * from table"; 
			// $query = $ci->db->query($sql);
			// $row = $query->result();
		$sql="SELECT * FROM user WHERE username='$user' AND password='$pas'";
		$query= $ci->db->query($sql);
		$row = $query->result();
			// $data=mysqli_query("select * from user where username='$user' and password='$pas'");
			// $da=mysqli_fetch_array($data);
		foreach($row as $r){}
			$word=$r->aksesmenu;		
		$word=explode(",", $word);
		$jword=count($word)-1;
		for($j=0;$j<=$jword;$j++)
		{
				// $tempa=mysql_query("select * from menu where id_menu='".$word[$j]."' order by urut");
				// $tempa = "";
			$tempa = $ci->db->query("SELECT * from menu where id_menu='".$word[$j]."' order by urut");
			$data = $tempa->result();
				// print_r($data);
			$jum = $tempa->num_rows();
				// $jum=mysql_num_rows($tempa);
			if($jum > 0)
			{
				foreach($data as $dt){}
					$level=$dt->parent;
				$name=$dt->nama;
				$id=$dt->id_menu;
				while($level >= 0)
				{
					if($level != 0 )
					{
						$q_nmp = $ci->db->query("select * from menu where id_menu='".$level."'  order by urut");
						$nmp = $q_nmp->result();
						$q_nmc = $ci->db->query("select * from menu where id_menu='".$id."' AND nama='".$name."'  order by urut");
						$nmc = $q_nmc->result();
							// $nmp=mysql_fetch_array(mysql_query(""));
							// $nmc=mysql_fetch_array(mysql_query(""));
						foreach($nmp as $nmp){}
							foreach($nmc as $nmc){}
								$namap=$nmp->nama;
							$am[$namap][]=$nmc->nama;
							
							$level=$nmp->id_menu;
							$level=$nmp->parent;
							$name=$nmp->nama;
							$id=$nmp->id_menu;
						}else
						{
							$level_1[]=$name;
							$level=-1;
						}
					}
				}
			}
			$level_1 = array_unique($level_1);
			$remove_null_number = true;
			
			echo menu(0,$h="",$am,$level_1);	
		}
		
		function edit_menu()
		{
			echo edit_menu_user(0,$h="");
		}
		
		function edit_menu_user($parent=0,$hasil)
		{
			$ci = &get_instance();
			$w = $ci->db->query("SELECT * from menu where parent='".$parent."' order by urut");
			// print_r($parent);
			if($parent!=0)
			{
				$hasil .= "";
			}

			foreach($w->result() as $h)
			{
				$x=$ci->db->query("SELECT * from menu where parent='".$h->id_menu."' order by urut");
				if(($x->num_rows())>0)
				{
					if($h->parent==0)
					{
						$hasil .="<div class='col-md-4'><table class='table table-bordered'>";
					}
					$icont=icon($h->nama);
					$hasil .='
					<tr><th colspan="2">
						<i style="margin-right:5px" class="fa '.$icont.'"></i>
						<span>'.$h->nama.'</span></th></tr>';
					}else
					{
						$cek=cek_status_menu($h->id_menu);
						$hasil .='<tr><td><i style="margin-right:5px" class="fa fa-file-text-o"></i>'.$h->nama.' </td><td><input name="cek[]" id="cek" value="'.$h->id_menu.'" '.$cek.' type="checkbox"/></td></tr>';
					}
					
					$hasil =edit_menu_user($h->id_menu,$hasil);
					$hasil .= "";
					if($h->parent==0)
					{
						$hasil .="</table></div>";
					}
				}
				if($parent!=0)
				{
					$hasil .= "";
				}
				return $hasil;
			}

			function cek_status_menu($id_menu)
			{
				$ci = &get_instance();
				$menu_user=$ci->Db_umum->select("select aksesmenu from user where no='".$ci->uri->segment(3)."' ");
				foreach($menu_user as $menu_user)
				{
					$menu1=$menu_user->aksesmenu;
				}

				$akses=explode(",",$menu1);
				$status="";
				foreach($akses as $akses)
				{
					if($akses==$id_menu)
					{
						$status="checked";
					}
				}

				return $status;
			}

			function icon($nama)
			{
				if($nama=="Setup data")
				{
					return "fa-wrench";
				}else
				if($nama=="Master Data")
				{
					return "fa-group";
				}else
				if($nama=="Transaksi")
				{
					return "fa-money";
				}else
				if($nama=="Laporan")
				{
					return "fa-list-alt";
				}else
				{
					return "fa-list-alt";
				}
			}

			function cari_link($no)
			{
				$ci = &get_instance();
				$query=$ci->db->query("select segment1,segment2 from menu where id_menu='".$no."'");
				$data=$query->result();
				foreach($data as $dt){}
					return $dt->segment1."/".$dt->segment2;
			}

			function cari_link_salah($no,$parent)
			{
				$ci = &get_instance();
				$query=$ci->db->query("select segment1,segment2 from menu where nama='".$no."' and parent='".$parent."'");
				$data=$query->result();
				foreach($data as $dt){}
					return $dt->segment1."/".$dt->segment2;
			}

			function search_parent($nama)
			{
				$ci = &get_instance();
				$query=$ci->db->query("select id_menu from menu where nama='".$nama."'");
				$data=$query->result();
				foreach($data as $dt){}
					return $dt->id_menu;
			}

		//ADALAH AKUN
			function edit_akun()
			{
				echo edit_akun_user(0,$h="");
			}

			function edit_akun_user($parent=0,$hasil)
			{
				$ci = &get_instance();
				$w = $ci->db->query("SELECT * from account where sub='".$parent."' order by sub");
			// print_r($parent);
				if($parent!=0)
				{
					$hasil .= "";
				}

				foreach($w->result() as $h)
				{
					$x=$ci->db->query("SELECT * from account where sub='".$h->id."' order by sub");
					if(($x->num_rows())>0)
					{
						if($h->sub==0)
						{
							$hasil .="<div class='col-md-4'>
							<div class='box box-warning collapsed-box'>
								<div class='box-header with-border'>
									<h3 class='box-title'>$h->nama</h3>
									<div class='box-tools pull-right'>
										<button class='btn btn-box-tool' data-widget='collapse'><i class='fa fa-plus'></i></button>
									</div>
								</div>
								<div class='box-body'>
									<table class='table table-bordered'>";
									}
									$icont=akun_icon($h->nama);
									$cek=cek_status_akun($h->idx);
									$hasil .='
									<tr><th colspan="2">
										<i style="margin-right:5px" class="fa '.$icont.'"></i>
										<span>'.$h->kodeakun.' '.$h->nama.'</span></th><td><input name="cek[]" id="cek" value="'.$h->idx.'" '.$cek.' type="checkbox"/></tr>';
									}else
									{
										$cek=cek_status_akun($h->idx);
										if($h->sub!=0){
											$hasil .='<tr><td><i style="margin-right:5px" class="fa fa-file-text-o"></i>'.$h->kodeakun.' '.$h->nama.'</td><td><input name="cek[]" id="cek" value="'.$h->idx.'" '.$cek.' type="checkbox"/></td></tr>';
										}
									}

									$hasil =edit_akun_user($h->id,$hasil);
									$hasil .= "";
									if($h->sub==0)
									{
										$hasil .="</table></div></div></div>";
									}
								}
								if($parent!=0)
								{
									$hasil .= "";
								}
								return $hasil;
							}

							function cek_status_akun($id_akun)
							{
								$ci = &get_instance();
								$akun_user=$ci->Db_umum->select("select coa from lembaga where id='".$ci->uri->segment(3)."' ");
								foreach($akun_user as $akun_user)
								{
									$akun1=$akun_user->coa;
								}

								$akses=explode(",",$akun1);
								$status="";
								foreach($akses as $akses)
								{
									if($akses==$id_akun)
									{
										$status="checked";
									}
								}

								return $status;
							}

							function akun_icon($nama)
							{
								if($nama=="Setup data")
								{
									return "fa-wrench";
								}else
								if($nama=="Master Data")
								{
									return "fa-group";
								}else
								if($nama=="Transaksi")
								{
									return "fa-money";
								}else
								if($nama=="Laporan")
								{
									return "fa-list-alt";
								}else
								{
									return "fa-list-alt";
								}
							}	

    function alert_stock()
    {
		$ci=& get_instance();
			// $sql = "select * from table"; 
			// $query = $ci->db->query($sql);
			// $row = $query->result();
		$sql="SELECT * FROM item WHERE item_stock < 10";
		$query= $ci->db->query($sql);
		$row = $query->result();

		foreach($row as $r)
		{
			?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                <strong><?=$r->item_name;?></strong> stock yang akan habis, harap segera lakukan pembelian.
              </div>
        <?php
		}
    }

}